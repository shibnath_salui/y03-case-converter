const fs = require('fs');

var folderPath = "D:\\XAMPP\\htdocs\\JWY03-BitBucket\\y03-testing";

fs.readdir( folderPath, (err, folderContent) => {
    
    if( err ) {
        reject( 'Exception on scanning directory at the location of ' + folderPath );
    } else {
        var cnt = 0;
        
        folderContent.forEach( subForder  => {
            
            if( subForder !== 'engine' &&  /^[A-Z]/.test( subForder) ) {
                
                //Renaming PDF
                rename( folderPath + '\\' + subForder + '\\media\\pdf\\' ).then( res => {
                    //Renaming VTT
                    rename( folderPath + '\\' + subForder + '\\media\\vtt\\' ).then( res => {
                        //Renaming video
                        rename( folderPath + '\\' + subForder + '\\media\\video\\' ).then( res => {
                            //Renaming folder
                            fs.renameSync( folderPath + '\\' + subForder , folderPath + '\\' + subForder.toLowerCase());

                            console.log( '['+ (cnt + 1) +']' + subForder + ': proceessed' )
                            cnt ++;
                        }).catch( err=> {
                            console.log( 'Exception in Video file renaming.', err );
                        })
                    }).catch( err=> {
                        console.log( 'Exception in VTT file renaming.', err );
                    })
                }).catch( err=> {
                    console.log( 'Exception in pdf file renaming.', err );
                })
                

                
            }
            
        });

        if( cnt == 0) {
            console.log('No such folder available which is containg uppercase letter!!!.');
        }
        
    }

});


async function rename( dirPath ) {
    try {
        return await new Promise((resolve, reject) => {

            fs.readdir( dirPath , function(err, files) {
                
                files.forEach(function(file) {
                    
                    fs.renameSync( dirPath + file, dirPath + file.toLowerCase());
                    
                });
                resolve(true)
            });
        })
    } catch (err) {
        reject( 'Exception in file renaming!!' )
    }
}