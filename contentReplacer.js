const fs = require('fs');
const replace = require('replace-in-file');

var path = "D:\\XAMPP\\htdocs\\JWY03-BitBucket\\y03-testing\\Ch03_L02_T03\\data\\content.js";


var folderPath = "D:\\XAMPP\\htdocs\\JWY03-BitBucket\\y03-testing";

fs.readdir( folderPath, (err, folderContent) => {
    
    if( err ) {
        reject( 'Exception on scanning directory at the location of ' + folderPath );
    } else {
        var cnt = 0;
        var i = 0;
        
        folderContent.forEach( subForder  => {
            
            if( subForder !== 'engine' &&  /^[A-Z]/.test( subForder) ) {

                var filePath = folderPath + '\\' + subForder + '\\data\\content.js';
                
                contentReplacer( filePath, subForder ).then( (res) => {
                    console.log( '['+ (cnt + 1) +'] File modified: ' + res );
                    cnt ++;
                    
                }).catch( err => {
                    console.log('Exception in content replacing...');
                })
            }
            
            if( cnt == 0 && i == folderContent.length - 1) {
                console.log('No such folder available which is containing uppercase letter!!!.');
            }
            
            i++;

        });
        
    }

});

async function contentReplacer( contentFilePath, chapterId) {
    return new Promise ( ( resolve, reject) => {

        const options = {

            //Single file
            files: contentFilePath,
          
            //Replacement to make (string or regex) 
            from: new RegExp(chapterId, "g"),
            to: chapterId.toLocaleLowerCase(),
        };
        
        replace(options).then(changedFiles => {
            resolve(changedFiles);
        }).catch(error => {
            console.error('Error occurred:', error);
        });

    })
}